---
title: About Me
permalink: false
---
# About Me
Officially Unofficial Pixie Wrangler (Badge Designer) for Cubcon
([@_cubcon](https://twitter.com/_cubcon)).
Embedded Security Engineer at Amazon Prime Air.
The opinions expressed here are my own and do not represent my employer.